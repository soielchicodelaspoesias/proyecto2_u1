#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import os
import gi
import  pymol
import  __main__
# Pymol: quiet  and no GUI
__main__.pymol_argv = ['pymol', '-qc']
pymol.finish_launching ()
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from dlg import dlgAxiliar


class Principal():
	def __init__(self):


		self.builder = Gtk.Builder()
		self.builder.add_from_file("./ui/ejemplo.ui")

		self.window = self.builder.get_object("wnMain")
		self.window.set_title("Ventana Ejemplo")
		self.window.resize(600, 400)
		self.window.connect("destroy", Gtk.main_quit)

		self.img = self.builder.get_object("openimagen")

		boton_about = self.builder.get_object("btnAbout")
		boton_about.connect("clicked", self.boton_about_clicked)

		boton_open = self.builder.get_object("btnOpen")
		boton_open.connect("clicked", self.boton_open_clicked)


		# label
		self.label_ejemplo = self.builder.get_object("labelEjemplo")

		# tree
		self.tree = self.builder.get_object("treeEjemplo")
		self.tree_model = Gtk.ListStore(*([str]))
		self.tree.set_model(model=self.tree_model)

		cell = Gtk.CellRendererText()
		column = Gtk.TreeViewColumn(title="Proteinas",
				            cell_renderer=cell,
				            text=0)

		self.tree.append_column(column)

		self.tree.connect("cursor-changed", self.liststore_changed)

		self.label = self.builder.get_object("label")

		self.direcciones = []
		self.archivos_pdb = []
		self.nombres_proteinas = []
		self.impresion = []

		self.window.show_all()



	def liststore_changed(self, tree=None):
		tree_model, it = self.tree.get_selection().get_selected()
		if tree_model is None or it is None:
			return

		self.impresion.append(tree_model.get_value(it, 0))
		self.label.set_text(tree_model.get_value(it, 0))
		self.crear_imagen()

	def boton_open_clicked(self, btn=None):

		dlg = dlgAxiliar(object_name="dlgFileChooser")
		response = dlg.dialogo.run()
		self.ruta = dlg.boton_open_clicked(self)
		dlg.dialogo.destroy()
		self.direcciones = os.listdir(self.ruta)

		for i in self.direcciones:
			if os.path.isfile(os.path.join(self.ruta, i)) and i.endswith(".pdb"):
			    self.archivos_pdb.append(i)

		for i in self.archivos_pdb:
			texto = ""
			for j in i:
				if j == ".":
					break
				texto += j
			self.nombres_proteinas.append(texto)

		self.llenar_liststore()
		dlg.dialogo.destroy()

	def llenar_liststore(self):

		for i in self.nombres_proteinas:
			self.tree_model.append([i])


	def boton_about_clicked(self, btn=None):

		dlg = dlgAxiliar(object_name="dlgAbout")
		dlg.dialogo.run()
		dlg.dialogo.destroy()


	def boton_ver_clicked(self, btn=None):

		archivo_png = self.ruta + "/" + self.impresion[0] + ".png"
		self.img.set_from_file(archivo_png)
		self.impresion = []


	def crear_imagen(self):

		pdb_file = self.ruta + "/" + self.impresion[0] + ".pdb"
		pdb_name = self.impresion[0]
		pymol.cmd.load(pdb_file , pdb_name)
		pymol.cmd.disable("all")
		pymol.cmd.enable(pdb_name)
		pymol.cmd.get_names ()
		pymol.cmd.hide('all')
		pymol.cmd.show('cartoon')
		pymol.cmd.set('ray_opaque_background', 0)
		pymol.cmd.pretty(pdb_name)
		pymol.cmd.png(self.ruta + "/" + pdb_name + ".png")
		pymol.cmd.ray()
		self.boton_ver_clicked()


if __name__ == "__main__":
	Principal()
	Gtk.main()
