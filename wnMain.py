#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from dlg import dlgAxiliar



class Principal():
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("./ui/ejemplo.ui")

		self.window = self.builder.get_object("wnMain")
		self.window.set_title("Ventana Ejemplo")
		self.window.resize(600, 400)
		self.window.connect("destroy", Gtk.main_quit)

		self.img = self.builder.get_object("openimagen")

		boton_about = self.builder.get_object("btnAbout")
		boton_about.connect("clicked", self.boton_about_clicked)

		boton_open = self.builder.get_object("btnOpen")
		boton_open.connect("clicked", self.boton_open_clicked)


		# label
		self.label_ejemplo = self.builder.get_object("labelEjemplo")

		# tree
		self.tree = self.builder.get_object("treeEjemplo")
		self.tree_model = Gtk.ListStore(*([str]))
		self.tree.set_model(model=self.tree_model)

		cell = Gtk.CellRendererText()
		column = Gtk.TreeViewColumn(title="Proteinas",
				            cell_renderer=cell,
				            text=0)

		self.tree.append_column(column)

		self.tree.connect("cursor-changed", self.liststore_changed)

		self.label = self.builder.get_object("label")

		self.direcciones = []

		self.window.show_all()





	def liststore_changed(self, tree=None):
		tree_model, it = self.tree.get_selection().get_selected()
		if tree_model is None or it is None:
			return

		self.direcciones[1] = tree_model.get_value(it, 0)
		texto = self.direcciones[0]
		contador_1 = 0
		contador_2 = 0
		for i in texto:
			if i == "/":
				contador_1 += 1
		texto2 = ""
		for i in texto:
			if contador_2 == contador_1:
				break
			if i == "/":
				contador_2 += 1
			texto2 += i
		self.direcciones[0] = texto2 + self.direcciones[1]
		self.label.set_text(self.direcciones[1])
		self.boton_ver_clicked()

	def crear_imagen(self):
		import  __main__
		# Pymol: quiet  and no GUI
		__main__.pymol_argv = ['pymol', '-qc']
		import  pymol
		pymol.finish_launching ()


		pdb_file = self.direcciones[1]
		pdb_name = self.direcciones[2]
		pymol.cmd.load(pdb_file , pdb_name)
		pymol.cmd.disable("all")
		pymol.cmd.enable(pdb_name)
		pymol.cmd.get_names ()
		pymol.cmd.hide('all')
		pymol.cmd.show('cartoon')
		pymol.cmd.set('ray_opaque_background', 0)
		pymol.cmd.pretty(pdb_name)
		pymol.cmd.png(pdb_name + ".png")
		pymol.cmd.quit()



	def boton_open_clicked(self, btn=None):
		dlg = dlgAxiliar(object_name="dlgFileChooser")
		dlg.dialogo.run()
		archivo = dlg.dialogo.get_filename()
		archivo_temp = archivo[::-1]
		texto = ""
		for i in archivo_temp:
			if i == "/":
				break
			texto += i
		nombre_archivo = texto[::-1]

		nombre_proteina = ""
		for i in nombre_archivo:
			if i == ".":
				break
			nombre_proteina += i

		# Condiciones por si no se abre un archivo.pdb
		if nombre_archivo != nombre_proteina + ".pdb":
			dlg.dialogo.destroy()
		elif nombre_archivo == nombre_proteina + ".pdb":
			self.tree_model.append([nombre_archivo])
			self.direcciones.append(archivo)
			self.direcciones.append(nombre_archivo)
			self.direcciones.append(nombre_proteina)
			dlg.dialogo.destroy()
			self.crear_imagen()


	def boton_about_clicked(self, btn=None):
		dlg = dlgAxiliar(object_name="dlgAbout")
		dlg.dialogo.run()
		dlg.dialogo.destroy()

	def boton_ver_clicked(self, btn=None):
		tree_model, it = self.tree.get_selection().get_selected()
		if tree_model is None or it is None:
			return

		archivo = self.direcciones[0]
		texto = ""
		contador = 0
		for i in archivo:
			if i == ".":
				break
			texto += i
		texto += ".png"
		self.img.set_from_file(texto)


if __name__ == "__main__":
	Principal()
	Gtk.main()

